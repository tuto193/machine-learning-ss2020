#! /usr/bin/python3
from math import log2

def entropy(s:list) -> float:
    """
    Calculate the entropy for a given target value set.

    Args:
        s (list): Target classes for specific observations.

    Returns:
        The entropy of s.
    """
    # Make sure we keep a dictionary that tracks all of them
    attr_values:dict = {}
    N:int = len(s)
    if N == 0:
        return 0

    for v in s:
        if not v in attr_values:
            attr_values[v] = 1
        else:
            attr_values[v] += 1
    # Get the sum of Entropies
    total:float = 0
    for v in attr_values:
        p:float= ( attr_values[v]/N)
        total +=  -p* log2(p)
    return total

def entropy_vals( pos:int, neg:int ) -> float:
    positive:int = pos
    negative:int = neg
    examples:list = []
    while positive > 0:
        examples.append(1)
        positive -= 1
    while negative > 0:
        examples.append(0)
        negative -= 1
    return entropy(examples)

def gain(targets, attr_values) -> float:
    """
    Calculates the expected reduction in entropy due to sorting on A.

    Args:
        targets (list): Target classes for observations in attr_values.
        attr_values (list): Values of each instance for the respective attribute.

    Returns:
        The information gain of
    """
    # YOUR CODE HERE
    ps:dict = {}
    for i, val in enumerate(attr_values):
        if val not in ps:
            my_list:list = []
            to_append = 1 if targets[i] == "yes" else 0
            my_list.append(to_append)
            ps[val] = my_list
        else:
            to_append = 1 if targets[i] == "yes" else 0
            ps[val].append(to_append)
    # get the sums
    sum_attr:float = 0
    for keys in ps:
        sum_attr += entropy(ps[keys]) * (len(ps[keys]) / len(targets))

    return entropy(targets) - sum_attr


from collections import Counter, namedtuple


class Node(namedtuple('Node', 'label children')):
    """
    A small node representation with a pretty string representation.
    """
    def __str__(self, level=0):
        return_str ='{}{!s}\n'.format(' ' * level * 4, self.label)
        for child in self.children:
            return_str += child.__str__(level + 1)
        return return_str

def id3(examples, attributes, target_attribute=None):
    """
    Calculate a tree of Nodes (fields: label [string], children [list])
    using the ID3 algorithm found as pseudocode on Wikipedia.
    """
    # YOUR CODE HERE
    if all(target == examples['targets'][0] for target in examples['targets']):
        return Node('Result: {!s}'.format(examples['target_names'][examples['targets'][0]]), [])

    # YOUR CODE HERE
    if len(attributes) == 0:
        attr = Counter(data_sample[target_attribute] for data_sample in examples['data']).most_common(1)
        return Node('Attribute: {!s}, {!s} occurences'.format(examples['attributes'][target_attribute], attr), [])

    # YOUR CODE HERE
    gains = [gain(examples['targets'], [r[attribute] for r in examples['data']])
             for attribute in attributes]
    max_gain_attribute = attributes[gains.index(max(gains))]

    # YOUR CODE HERE
    root = Node('Attribute: {!s} (gain {!s})'.format(examples['attributes'][max_gain_attribute],
                                                     round(max(gains), 4)), [])

    # YOUR CODE HERE
    for vi in set(data_sample[max_gain_attribute] for data_sample in examples['data']):
        # YOUR CODE HERE
        child = Node('Value: {!s}'.format(vi), [])
        root.children.append(child)

        # YOUR CODE HERE
        vi_indices = [idx for idx, data_sample in enumerate(examples['data'])
                          if data_sample[max_gain_attribute] == vi]
        examples_vi = dict(examples)
        examples_vi['data'] = [examples['data'][i] for i in vi_indices]
        examples_vi['targets'] = [examples['targets'][i] for i in vi_indices]

        if examples_vi['data']:
            # YOUR CODE HERE
            child.children.append(
                id3(examples_vi,
                    [attribute_ for attribute_ in attributes if not attribute_ == max_gain_attribute],
                    max_gain_attribute)
            )

        else:
            # YOUR CODE HERE
            attr = Counter(examples_vi['targets']).most_common(1)
            label = 'Attribute: {!s}, {!s} occurences'.format(examples['attributes'][target_attribute], attr)
            child.children.append(Node(label, []))

    return root
