import numpy as np
import matplotlib.pyplot as plt

def pnorm(x, p):
    """
    Calculates the p-norm of x.

    Args:
        x (array): the vector for which the norm is to be computed.
        p (float): the p-value (a positive real number).

    Returns:
        The p-norm of x.
    """
    result = None
    # YOUR CODE HERE
    # loop over each component of the vector
    inner_sum = 0
    if isinstance(x, (int, np.integer)) or isinstance(x, (float, np.floating)):
        # calculate the abs of x
        extension = np.abs(x)**p
        # abs of x to the power of p
        # return x to the power of (1/p)
        result = extension**(1/p)
    else:
        # so, if x is not an int
        for x_i in x:
            # calculate the abs of the component
            extension = np.abs(x_i)**p
            # abs of the component to the power of p
            # sum over all components
            inner_sum += extension
            # return the sum to the power of (1/p)
            result = inner_sum**(1/p)

    return result


def pdist(x0, x1, p=2):
    """
    Calculates the distance between x0 and x1
    using the p-norm.

    Arguments:
        x0 (array): the first vector.
        x1 (array): the second vector.
        p (float): the p-value (a positive real number).

    Returns:
        The p-distance between x0 and x1.
    """
    result = None
    # YOUR CODE HERE
    # difference between vectors
    difference_vector = []
    if isinstance(x0, (int, np.integer)) or isinstance(x0, (float, np.floating)):
        difference_vector = np.abs(x0-x1)
    else:
        for x0_i,x1_i in zip(x0, x1):
            difference_vector.append(np.abs(x0_i-x1_i))
    # p-norm of difference
    result = pnorm(difference_vector,p)
    return result


def kmeans(data, k=3):
    """
    Applies kmeans clustering to the data using k initial clusters.
    data is expected to be a numpy array of size n*2,
    n being the amount of observations in the data. This function returns
    the centroids and the labels for the clusters data (1,1,3,5,5,5,...)

    Args:
        data(list/np.array) : the  data points to be clustered
        k (int): the amount of clusters wanted

    Returns:
    (list, np.array): list with labels for the data points (ordered) and
            the centroids of the clusters
    """
    # YOUR CODE HERE
    # the dimensions of the data and the vectors needed
    dimensions:int = data[0].size
    #  get the boundaries for the vectors
    # boundaries:list = []
    # We will only need the t-th(1) and (t-1)-th(0) set of vectors
    K_vecs0 = np.zeros((k, dimensions))
    K_vecs1 = np.zeros((k, dimensions))
    epsilon = 0.0001
    for d in range(dimensions):
        # fill the W_t(0)s
        lowest = min(data[:,d])
        highest = max(data[:,d])
        # boundaries.append([lowest, highest])
        n_column = np.random.uniform(low=lowest, high=highest, size=k)
        n_column = np.array(n_column)
        # if d == 0:
        K_vecs1[:,d] += n_column
        # else:
            # np.hstack((K_vecs1, n_column))
    labels:str = []
    colors:list = []
    for cluster in range(k):
        colors.append(np.random.rand(3,).tolist())
    while(True):
        # initialize the  labels
        labels = []
        # Initialize our set of clusters
        K_clusters:list = []
        for d in range(k):
            # Initialize the clusters
            K_clusters.append(None)
        # assign each data point to the closest cluster to itself
        for x_i in data[:]:
            closest_Kluster:int = 0
            min_distance:np.array = pdist(x_i,K_vecs1[0])
            # find the closest cluster to the data point
            for k_vec_i, vec in enumerate(K_vecs1.tolist()):
                # ignore the first index
                if k_vec_i == 0:
                    continue
                dist = pdist(x_i, vec)
                if dist < min_distance:
                    min_distance = dist
                    closest_Kluster = k_vec_i
            if None != K_clusters[closest_Kluster]:
                K_clusters[closest_Kluster].append(x_i)
            else:
                K_clusters[closest_Kluster] = [x_i]
            #  Add the label  to the data point
            labels.append(closest_Kluster)
            # labels.append(colors[closest_Kluster])

        K_clusters = np.array(K_clusters)
        # t++
        K_vecs0[:] = K_vecs1[:]
        # Update our W_k()s
        for index, k in enumerate(K_clusters.tolist()):
            # Not all clusters might have changed
            if k != None:
                # Assign the  centroids fo the clusters
                K_vecs1[index] = np.mean(k)
        # If at least one of the clusters changed, repeat all
        for t0, t1 in zip(K_vecs0[:], K_vecs1[:]):
            if pdist(t0, t1) > epsilon:
                continue
        # else we are done
        break
    centroids = K_vecs1
    return labels, centroids



data = np.loadtxt('clusterData.txt')


# Test different ks here
# YOUR CODE HERE
for k in [3, 7]:
    labels, centroids = kmeans(data, k)

    kmeans_fig = plt.figure('k-means with k={}'.format(k))
    plt.scatter(data[:,0], data[:,1], c=labels)
    plt.scatter(centroids[:,0], centroids[:,1],
                c=list(set(labels)), alpha=.1, marker='o',
                s=np.array([len(labels[labels==label]) for label in set(labels)])*100)
    plt.title('k = '+str(k))
    kmeans_fig.canvas.draw()
