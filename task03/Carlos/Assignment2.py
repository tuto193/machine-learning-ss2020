import numpy as np

def pnorm(x:list, p:float):
    """
    Calculates the p-norm of x.

    Args:
        x (array): the vector for which the norm is to be computed.
        p (float): the p-value (a positive real number).

    Returns:
        The p-norm of x.
    """
    if p == 0:
        raise ArithmeticError
    result = 0
    # YOUR CODE HERE
    if not isinstance(x, list):
        result += np.power(x, p)
    else:
        for val in x:
            result += np.power(val, p)
    return np.power(result, 1.0/p)


# 1e-10 is 0.0000000001
assert pnorm(1, 2)      - 1          < 1e-10 , "pnorm is incorrect for x = 1, p = 2"
assert pnorm(2, 2)      - 2          < 1e-10 , "pnorm is incorrect for x = 2, p = 2"
assert pnorm([2, 1], 2) - np.sqrt(5) < 1e-10 , "pnorm is incorrect for x = [2, 1], p = 2"
assert pnorm(2, 0.5)    - 2          < 1e-10 , "pnorm is incorrect for x = 2, p = 0.5"

#########################################################
#########################################################
#########################################################

def pdist(x0:list, x1:list, p:float):
    """
    Calculates the distance between x0 and x1
    using the p-norm.

    Arguments:
        x0 (array): the first vector.
        x1 (array): the second vector.
        p (float): the p-value (a positive real number).

    Returns:
        The p-distance between x0 and x1.
    """
    np_x0 = np.array(x0)
    np_x1 = np.array(x1)
    if np_x0.size != np_x1.size or p == 0:
        raise ArithmeticError
    # result = None
    # YOUR CODE HERE
    np_distance = np_x0 - np_x1

    return pnorm(np_distance.tolist(), p)


# 1e-10 is 0.0000000001
assert pdist(1, 2, 2)           - 1          < 1e-10 , "pdist is incorrect for x0 = 1, x1 = 2, p = 2"
assert pdist(2, 5, 2)           - 3          < 1e-10 , "pdist is incorrect for x0 = 2, x1 = 5, p = 2"
assert pdist([2, 1], [1, 2], 2) - np.sqrt(2) < 1e-10 , "pdist is incorrect for x0 = [2, 1], x1 = [1, 2], p = 2"
assert pdist([2, 1], [0, 0], 2) - np.sqrt(5) < 1e-10 , "pdist is incorrect for x0 = [2, 1], x1 = [0, 0], p = 2"
assert pdist(2, 0, 0.5)         - 2          < 1e-10 , "pdist is incorrect for x0 = 2, x1 = 0, p = 0.5"

#########################################################
#########################################################
#########################################################

# %matplotlib notebook
# import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ColorConverter


color = ColorConverter()
figure_norms = plt.figure('p-norm comparison')

# create the linspace vector
# YOUR CODE HERE
ls = np.linspace(start=-100, stop=100,num=50,endpoint=True)

assert len(ls) == 50 , 'ls should be of length 50.'
assert (min(ls), max(ls)) == (-100, 100) , 'ls should range from -100 to 100, inclusively.'

for i, p in enumerate([1.0/8, 1.0/4, 1.0/2, 1.0, 1.5, 2.0, 4.0, 8.0, 128.0]):
    # Create a numpy array containing useful values instead of zeros.
    data = np.zeros((2500, 3))
    # YOUR CODE HERE
    #Cartesian product calculation taken from Stack Overflow
    cartesian_prod = np.transpose([np.tile(ls, ls.size), np.repeat(ls, ls.size)])
    #Assign the values, so it's no longer just 0s
    data[:,:2] += cartesian_prod

    # calculate the pnorm of each sub-array
    for i, arr in enumerate(cartesian_prod.tolist()):
        data[i,2] = pnorm(arr, p)

    assert data[100,2]>0.9 and data[100,2]<1, "Wrong result for p norm, make sure you use NORM and not pdist!"
    assert all(data[:,2] <= 1), 'The third column should be normalized.'

    # Plot the data.
    colors = [color.to_rgb((1, 1-a, 1-a)) for a in data[:,2]]
    a = plt.subplot(3, 3, i + 1)
    plt.scatter(data[:,0], data[:,1], marker='.', color=colors)
    a.set_ylim([-100, 100])
    a.set_xlim([-100, 100])
    a.set_title('{:.3g}-norm'.format(p))
    a.set_aspect('equal')
    plt.tight_layout()
    figure_norms.canvas.draw()
