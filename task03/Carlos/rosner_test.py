%matplotlib notebook
import numpy as np
import matplotlib.pyplot as plt
import math

# generate dataset
data = list(zip(np.random.uniform(size=100), np.random.normal(size=100)))
data += list(zip(np.random.uniform(size=10), np.random.normal(0, 10, size=10)))
data = np.array(data)
outliers = []

# just to check if everything is pretty
fig_rosner_data = plt.figure('The Dataset')
plt.scatter(data[:,0], data[:,1], marker='x')
plt.axis([0, 1, -20, 20])
fig_rosner_data.canvas.draw()

# Now find the outliers, add them to 'outliers', and remove them from 'data'.
# YOUR CODE HERE
# determine the median/mean to calculate deviation
median:float = 0
mean:float = 0
st_dev_median:float = 0
st_dev_mean:float = 0
threshhold:float = 3.5
sorted_y = data[:,1]
sorted_y = np.sort(sorted_y)
n_data:int = len(sorted_y)
#Get the average value (mean)
mean = data.mean()
#Get the median
half_n:int = math.floor(n_data / 2)
# The median is the value right in the middle or the average of the too middle ones
if( n_data % 2 != 0):
    median = sorted_y[half_n + 1]
else:
    median = (sorted_y[half_n] + sorted_y[half_n + 1]) / 2

# Standard deviation
for xi in np.nditer(data):
    st_dev_mean +=  (xi[1] - mean)**2
    st_dev_median += (xi[1] - median)**2

st_dev_mean /= n_data
st_dev_mean = math.sqrt(st_dev_mean)

st_dev_median /= n_data
st_dev_median = st_dev_median**0.5

# Detect outliers
n_data -= 1
while( n_data >= 0 ):
    check = (data[n_data,:] - median) / st_dev_median
    if(check[1] > 3.5):
        outliers.insert(0, data[-1,:])
        data = data[:-1]
    n_data -= 1

# plot results
outliers = np.array(outliers)
fig_rosner = plt.figure('Rosner Result')
plt.scatter(data[:,0], data[:,1], c='b', marker='x', label='cleared data')
plt.scatter(outliers[:,0], outliers[:,1], c='r', marker='x', label='outliers')
plt.axis([0, 1, -20, 20])
plt.legend(loc='lower right');
fig_rosner.canvas.draw()
