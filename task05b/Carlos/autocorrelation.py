import numpy as np

def autocorrelation(data):
    """
    Input is an array of the form number_data * dimensions
    An example is shown when this cell is executed
    """
    # YOUR CODE HERE
    # means for covariance matrix
    dimensions:int = data.size
    mu_s = []
    std_s = []
    variance_s = []
    autocor_mat = np.zeros((dimensions, dimensions))
    for i, value in enumerate(data.tolist()):
        for j, value2 in enumerate(data.tolist()):
            mean_X = np.mean(data[i] * data[j])
            mean_x1 = np.mean(data[i])
            mean_x2 = np.mean(data[i])
            autocor_mat[i, j] = mean_X - (mean_x1 * mean_x2)
        # standard_dev = np.std(data[i])
        # mean_mu = np.mean(data[i])
        mu_s.append(np.mean(data[i]))
        std_s.append(np.std(data[i]))
        variance_s.append(np.var(data[i], ddof=1))
    mu_s = np.array(mu_s)
    variance_s = np.array(variance_s)
    std_s = np.array(std_s)

    muT_mu = mu_s.T @ mu_s
    covariance_s = variance_s.T @ variance_s
    stds_mat = std_s.T @ std_s
    # the autocovariance of the means



    return np.corrcoef(data[2], data[1])


## DELETE COMMENTS TO SHOW THE DATA USED FOR ASSERTION
#example_x = [2,2,2]
#example_y = [1,2,3]
#example_data = np.stack([[1,2,3], [2,2,2]], axis=1)
#plt.scatter(example_x, example_y)
#plt.show()

test_data1 = np.stack([[1,2,3], [2,2,2]], axis=1)
cor1 = autocorrelation(test_data1)
real_cor1 = [[1.0, 0.0], [0.0, 0.0]]

test_data2 = np.stack([[1,2,3], [4,5,6]], axis=1)
cor2 = autocorrelation(test_data2)
real_cor2 = [[1,1], [1,1]]

assert np.allclose(cor1, real_cor1), "Wrong output: correlation should be \n {} \n for this data, is \n {}".format(real_cor1, cor1)
assert np.allclose(cor2, real_cor2), "Wrong output: correlation should be \n {} \n for this data, is \n {}".format(real_cor2, cor2)
