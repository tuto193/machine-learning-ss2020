# Initialization
condition_db = [{"food": 1, "bell": 0},
                {"food": 0, "bell": 1},
                {"food": 1, "bell": 1}]

trials = [0, 1, 2, 1, 2, 1, 2, 1]

epsilon = 0.2
theta = 1/2

responses = []
weight_food = [1]
weight_bell = [0]

# TODO: For each trial, update the current weights of the US and CS and store
# the results in the respective lists. Also store the response.
# YOUR CODE HERE

#start
for t in trials:
    # taking from db above
    x_1 = condition_db[t]["food"]
    x_2 = condition_db[t]["bell"]
    #to access current weight, using last item on the list
    w_1 = weight_food[-1]
    w_2 = weight_bell[-1]

    r = x_1 * w_1 + x_2 * w_2
    r = 1 if r >= 0 else 0
    responses.append(int(r))
    #calculate both w's
    weight_food.append(w_1 + epsilon * r * x_1)
    weight_bell.append(w_2 + epsilon * r * x_2)



# Output
print("| Food   |   |" + "|   |".join(["{:3d}".format(condition_db[trial]["food"]) for trial in trials]) + "|   |")
print("| Bell   |   |" + "|   |".join(["{:3d}".format(condition_db[trial]["bell"]) for trial in trials]) + "|   |")
print("| Saliva |   |" + "|   |".join(["{:3d}".format(response) for response in responses]) + "|   |")
print("| w_Food |" + "|   |".join(["{:3.1f}".format(w) for w in weight_food]) + "|")
print("| w_Bell |" + "|   |".join(["{:3.1f}".format(w) for w in weight_bell]) + "|")
