def is_consistent(h, datum) -> bool:
    """
    Checks if a general hypothesis is consistent with a datum.

    Args:
        h (tuple): Hypothesis.
        datum (dict): Datum with values and target.

    Returns:
        bool: Whether the hypothesis correctly predicts the target value.
    """
    # YOUR CODE HERE
    for i, val in enumerate(datum["values"]):
        # if its general we don't really care
        if h[i] == "?":
            continue
        # if the hypothesis does not match the data
        elif (val != h[i]):
            # only if its specific and the target and hypothesis don't match up
            if datum["target"]:
                return False
            else:
                continue
    return True

assert is_consistent(('far', 'cheap', '?', 'no'), {'values': ('far', 'cheap', 'many', 'no' ), 'target': True })

def is_more_general_or_equal(h1, h2):
    """
    Checks whether hypothesis h1 is more general than hypothesis h2 or equally general.

    Args:
        h1 (tuple): Hypothesis 1.
        h1 (tuple): Hypothesis 2.

    Returns:
        bool: True if the predicate is satisfied.
    """
    # YOUR CODE HERE
    counter_1:int = 0
    counter_2:int = 0
    for i, val in enumerate(h1):
        if val == "?":
            counter_1 += 1
        if h2[i] == "?":
            # If they are different they would clasify differently too
            if val != "?":
                return False
            counter_2 += 1
    return counter_1 >= counter_2


assert is_more_general_or_equal(('?', '?', '?', '?'), ('far', 'cheap', 'many', 'no'))
assert not is_more_general_or_equal(('?', '?', 'many', 'no'), ('far', 'cheap', 'many', '?'))

def is_more_specific_or_equal(h1, h2):
    """
    Checks whether hypothesis h1 is more specific than hypothesis h2 or equally specific.

    Args:
        h1 (tuple): Hypothesis 1.
        h1 (tuple): Hypothesis 2.

    Returns:
        bool: True if the predicate is satisfied.
    """
    # YOUR CODE HERE
    counter_1:int = 0
    counter_2:int = 0
    for i, val in enumerate(h1):
        if val != "?":
            counter_1 += 1
        if h2[i] != "?":
            # If they are different they would clasify differently too
            if val == "?":
                return False
            counter_2 += 1
    return counter_1 >= counter_2


assert is_more_specific_or_equal(('?', '?', 'many', 'no'), ('?', '?', 'many', '?'))
assert not is_more_specific_or_equal(('?', 'cheap', 'many', 'no'), ('far', '?', '?', '?'))

def generalize_minimally(h, datum):
    """Generalize hypothesis h so it becomes consitent with the datum.

    Args:
        h (tuple): The hypothesis to be generalized.
        datum (tuple): Attribute values of a datum. The datum is assumed to have a positive target value.

    Returns:
        tuple: The generalized hypothesis.
    """
    # YOUR CODE HERE
    general_h = list(h)
    for i, val in enumerate(h):
        if val != "?":
            if val == "0":
                general_h[i] = datum[i]
            elif datum[i] != val and val:
                general_h[i] = "?"
                break
    return tuple(general_h)

assert generalize_minimally(('?', '?', 'many', 'no'), ('short', 'cheap', 'many', 'yes')) == ('?', '?', 'many', '?')

def specialize_minimally(h, datum, attr_values):
    """
    Generate all consistent minimal specialization of hypothesis h.

    Args:
        h (tuple): The hypothesis to be specialized.
        datum (tuple): Attribute values of a datum. The datum is assumed to have a negative target value.
        attr_values (tuple of tuples): All possible attribute values for each attribute.

    Returns:
        tuple of tuples: Tuple of the specialized hypotheses.
    """
    # YOUR CODE HERE
    new_h:list = []
    for i, val in enumerate(h):
        hypothesis:list = list(h)
        if val == "?":
            dont_x = datum[i] # This is the opposite of what we want to assign to our H
            new_x:str = ""
            for j, atr in enumerate(attr_values):
                if i == j: # we make sure we are at the right attribute
                    if atr[0] == dont_x:
                        new_x = atr[1]
                    else:
                        new_x = atr[0]
            hypothesis[i] = new_x
            new_h.append(tuple(hypothesis))
    return tuple(new_h)


attr_values = (('short', 'far'), ('cheap', 'expensive'), ('many', 'none'), ('yes', 'no'))
assert specialize_minimally(('?', '?', 'many', 'no'), ('short', 'cheap', 'many', 'no'), attr_values) == (('far', '?', 'many', 'no'), ('?', 'expensive', 'many', 'no'))

def eliminate_candidates(data, attr_values):
    """
    Candidate elimination algorithm printing its progress at each step.

    Args:
        data (list of dicts): The dataset.
        attr_values (tuple of tuples): All possible attribute values for each attribute in the data.

    Returns:
        tuple: The general and the specific boundary of the version space.
    """
    # Initialize general and specific boundary.

    # Maximally general hypothesis.
    general_boundary = [('?', '?', '?', '?')]
    # Maximally specific hypothesis.
    specific_boundary = [('0', '0', '0', '0')]

    # Fit the version space to the data.
    for datum in data:
        if datum['target']:
            # If the sample is classified as positive...

            # Remove all inconsistent hypotheses from G.
            general_boundary = [g for g in general_boundary if is_consistent(g, datum)]

            for s in specific_boundary:
                # Remove all inconsistent hypothesis from S.
                if not is_consistent(s, datum):
                    specific_boundary.remove(s)

                    # Add to S all minimal generalizations s. In this case only one.
                    s_generalized = generalize_minimally(s, datum['values'])
                    # Add the new hypothesis to the specific boundary, if it is not more general
                    # than the general boundary. We do not need to check for consistency again
                    # as the hypothesis was constructed in such a way that it must be consistent.
                    if any(is_more_general_or_equal(g, s_generalized) for g in general_boundary):
                        specific_boundary.append(s_generalized)

            # Remove from S any hypothesis that is more general than another hypothesis in S.
            for s in specific_boundary:
                if any(is_more_general_or_equal(s, s2)
                       and not s == s2 for s2
                       in specific_boundary):

                    specific_boundary.remove(s)

        else:
            # If the sample is classified as negative...

            # Remove all inconsistent hypotheses from S.
            specific_boundary = [s for s in specific_boundary if is_consistent(s, datum)]
            for g in general_boundary:
                # Remove all inconsistent hypotheses from G.
                if not is_consistent(g, datum):
                    general_boundary.remove(g)

                # Add to G all minimal specializations of g.
                for specialized_g in specialize_minimally(g, datum['values'], attr_values):
                # Add the new specialized hypothesis to the general boundary, if it is not more
                # specific than the specific boundary.
                # We do not need to check for consistency again
                # as the hypothesis was constructed in such a way that it must be consistent.
                    if any(is_more_specific_or_equal(s, specialized_g) for s in specific_boundary):
                            general_boundary.append(specialized_g)

            # Remove from G any hypothesis that is less general than another hypothesis in G.
            for g in general_boundary:
                if any(is_more_specific_or_equal(g, g2)
                        and not g == g2 for g2
                        in general_boundary):

                    general_boundary.remove(g)

        # Print progress of algorithm at each iteration.
        print('Sample: {} {}\nS: {}'.format('+' if datum['target'] else '-', datum['values'],
                                                     specific_boundary))
        print("G:[")
        for g in general_boundary:
            print(g)
        print("]\n")

    return general_boundary, specific_boundary

attr_values = (('short', 'far'), ('cheap', 'expensive'), ('many', 'none'), ('yes', 'no'))

# samples
data = [
    {'values': ('far',   'cheap',     'many', 'no' ), 'target': True },
    {'values': ('short', 'expensive', 'many', 'no' ), 'target': True },
    {'values': ('far',   'expensive', 'none', 'yes'), 'target': False},
    {'values': ('short', 'cheap',     'none', 'yes'), 'target': False},
    {'values': ('short', 'cheap',     'many', 'yes'), 'target': True }
]

eliminate_candidates(data, attr_values)
