import numpy as np
# %matplotlib inline
import pandas as pd
import seaborn as sns

# TODO: Load the cars dataset in cars.csv .
# YOUR CODE HERE
cars = np.genfromtxt("cars.csv", delimiter=",")
assert cars.shape == (97, 11), "Shape is not (97, 11), was {}".format(cars.shape)


# sns.set()
# cols = ['Suggested retail price (USD)', 'Price to dealer (USD)',
#           'Engine size (liters)', 'Number of engine cylinders',
#           'Engine horsepower', 'City gas mileage' ,
#           'Highway gas mileage', 'Weight (pounds)',
#           'Wheelbase (inches)', 'Length (inches)', 'Width (inches)']

# df = pd.DataFrame(cars, columns=cols)
# sns.pairplot(df)


# import numpy as np

# TODO: Normalize the data and store them in a variable called cars_norm.
# YOUR CODE HERE
mean_cars = cars.mean()
cars_std = np.std(cars)

cars_norm = (cars - mean_cars) / cars_std

assert cars_norm.shape == (97, 11), "Shape is not (97, 11), was {}".format(cars.shape)
assert np.abs(np.sum(cars_norm)) < 1e-10, "Absolute sum was {} but should be close to 0".format(np.abs(np.sum(cars_norm)))
assert np.abs(np.sum(cars_norm ** 2) / cars_norm.size - 1) < 1e-10, "The data is not normalized, sum/N was {} not 1".format(np.sum(cars_norm ** 2) / cars_norm.size)
